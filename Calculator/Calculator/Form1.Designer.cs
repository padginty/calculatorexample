﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNumber1 = new System.Windows.Forms.Button();
            this.btnNumber4 = new System.Windows.Forms.Button();
            this.btnNumber7 = new System.Windows.Forms.Button();
            this.btnNumber2 = new System.Windows.Forms.Button();
            this.btnNumber5 = new System.Windows.Forms.Button();
            this.btnNumber8 = new System.Windows.Forms.Button();
            this.btnPlus = new System.Windows.Forms.Button();
            this.btnMinus = new System.Windows.Forms.Button();
            this.btnEqual = new System.Windows.Forms.Button();
            this.btnNumber3 = new System.Windows.Forms.Button();
            this.btnNumber6 = new System.Windows.Forms.Button();
            this.btnNumber9 = new System.Windows.Forms.Button();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.btnNumber0 = new System.Windows.Forms.Button();
            this.btnCLear = new System.Windows.Forms.Button();
            this.btnMultiply = new System.Windows.Forms.Button();
            this.btnDivide = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNumber1
            // 
            this.btnNumber1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber1.Location = new System.Drawing.Point(36, 121);
            this.btnNumber1.Name = "btnNumber1";
            this.btnNumber1.Size = new System.Drawing.Size(67, 64);
            this.btnNumber1.TabIndex = 0;
            this.btnNumber1.Text = "1";
            this.btnNumber1.UseVisualStyleBackColor = false;
            this.btnNumber1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber4
            // 
            this.btnNumber4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber4.Location = new System.Drawing.Point(36, 191);
            this.btnNumber4.Name = "btnNumber4";
            this.btnNumber4.Size = new System.Drawing.Size(67, 64);
            this.btnNumber4.TabIndex = 0;
            this.btnNumber4.Text = "4";
            this.btnNumber4.UseVisualStyleBackColor = false;
            this.btnNumber4.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber7
            // 
            this.btnNumber7.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber7.Location = new System.Drawing.Point(36, 261);
            this.btnNumber7.Name = "btnNumber7";
            this.btnNumber7.Size = new System.Drawing.Size(67, 64);
            this.btnNumber7.TabIndex = 0;
            this.btnNumber7.Text = "7";
            this.btnNumber7.UseVisualStyleBackColor = false;
            this.btnNumber7.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber2
            // 
            this.btnNumber2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber2.Location = new System.Drawing.Point(109, 121);
            this.btnNumber2.Name = "btnNumber2";
            this.btnNumber2.Size = new System.Drawing.Size(67, 64);
            this.btnNumber2.TabIndex = 0;
            this.btnNumber2.Text = "2";
            this.btnNumber2.UseVisualStyleBackColor = false;
            this.btnNumber2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber5
            // 
            this.btnNumber5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber5.Location = new System.Drawing.Point(109, 191);
            this.btnNumber5.Name = "btnNumber5";
            this.btnNumber5.Size = new System.Drawing.Size(67, 64);
            this.btnNumber5.TabIndex = 0;
            this.btnNumber5.Text = "5";
            this.btnNumber5.UseVisualStyleBackColor = false;
            this.btnNumber5.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber8
            // 
            this.btnNumber8.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber8.Location = new System.Drawing.Point(109, 261);
            this.btnNumber8.Name = "btnNumber8";
            this.btnNumber8.Size = new System.Drawing.Size(67, 64);
            this.btnNumber8.TabIndex = 0;
            this.btnNumber8.Text = "8";
            this.btnNumber8.UseVisualStyleBackColor = false;
            this.btnNumber8.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnPlus
            // 
            this.btnPlus.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnPlus.Location = new System.Drawing.Point(255, 331);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(67, 64);
            this.btnPlus.TabIndex = 0;
            this.btnPlus.Text = "+";
            this.btnPlus.UseVisualStyleBackColor = false;
            this.btnPlus.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnMinus
            // 
            this.btnMinus.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnMinus.Location = new System.Drawing.Point(255, 261);
            this.btnMinus.Name = "btnMinus";
            this.btnMinus.Size = new System.Drawing.Size(67, 64);
            this.btnMinus.TabIndex = 0;
            this.btnMinus.Text = "-";
            this.btnMinus.UseVisualStyleBackColor = false;
            this.btnMinus.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnEqual
            // 
            this.btnEqual.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnEqual.Location = new System.Drawing.Point(182, 331);
            this.btnEqual.Name = "btnEqual";
            this.btnEqual.Size = new System.Drawing.Size(67, 64);
            this.btnEqual.TabIndex = 0;
            this.btnEqual.Text = "=";
            this.btnEqual.UseVisualStyleBackColor = false;
            this.btnEqual.Click += new System.EventHandler(this.btnEqual_Click);
            // 
            // btnNumber3
            // 
            this.btnNumber3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber3.Location = new System.Drawing.Point(182, 121);
            this.btnNumber3.Name = "btnNumber3";
            this.btnNumber3.Size = new System.Drawing.Size(67, 64);
            this.btnNumber3.TabIndex = 0;
            this.btnNumber3.Text = "3";
            this.btnNumber3.UseVisualStyleBackColor = false;
            this.btnNumber3.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber6
            // 
            this.btnNumber6.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber6.Location = new System.Drawing.Point(182, 191);
            this.btnNumber6.Name = "btnNumber6";
            this.btnNumber6.Size = new System.Drawing.Size(67, 64);
            this.btnNumber6.TabIndex = 0;
            this.btnNumber6.Text = "6";
            this.btnNumber6.UseVisualStyleBackColor = false;
            this.btnNumber6.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnNumber9
            // 
            this.btnNumber9.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber9.Location = new System.Drawing.Point(182, 261);
            this.btnNumber9.Name = "btnNumber9";
            this.btnNumber9.Size = new System.Drawing.Size(67, 64);
            this.btnNumber9.TabIndex = 0;
            this.btnNumber9.Text = "9";
            this.btnNumber9.UseVisualStyleBackColor = false;
            this.btnNumber9.Click += new System.EventHandler(this.btn_Click);
            // 
            // txtDisplay
            // 
            this.txtDisplay.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplay.Location = new System.Drawing.Point(21, 31);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.ReadOnly = true;
            this.txtDisplay.Size = new System.Drawing.Size(318, 45);
            this.txtDisplay.TabIndex = 1;
            this.txtDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnNumber0
            // 
            this.btnNumber0.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnNumber0.Location = new System.Drawing.Point(109, 331);
            this.btnNumber0.Name = "btnNumber0";
            this.btnNumber0.Size = new System.Drawing.Size(67, 64);
            this.btnNumber0.TabIndex = 0;
            this.btnNumber0.Text = "0";
            this.btnNumber0.UseVisualStyleBackColor = false;
            this.btnNumber0.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnCLear
            // 
            this.btnCLear.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnCLear.Location = new System.Drawing.Point(36, 331);
            this.btnCLear.Name = "btnCLear";
            this.btnCLear.Size = new System.Drawing.Size(67, 64);
            this.btnCLear.TabIndex = 0;
            this.btnCLear.Text = "C";
            this.btnCLear.UseVisualStyleBackColor = false;
            this.btnCLear.Click += new System.EventHandler(this.btnCLear_Click);
            // 
            // btnMultiply
            // 
            this.btnMultiply.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnMultiply.Location = new System.Drawing.Point(255, 191);
            this.btnMultiply.Name = "btnMultiply";
            this.btnMultiply.Size = new System.Drawing.Size(67, 64);
            this.btnMultiply.TabIndex = 0;
            this.btnMultiply.Text = "*";
            this.btnMultiply.UseVisualStyleBackColor = false;
            this.btnMultiply.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // btnDivide
            // 
            this.btnDivide.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnDivide.Location = new System.Drawing.Point(255, 121);
            this.btnDivide.Name = "btnDivide";
            this.btnDivide.Size = new System.Drawing.Size(67, 64);
            this.btnDivide.TabIndex = 0;
            this.btnDivide.Text = "/";
            this.btnDivide.UseVisualStyleBackColor = false;
            this.btnDivide.Click += new System.EventHandler(this.btnOperator_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(357, 426);
            this.Controls.Add(this.txtDisplay);
            this.Controls.Add(this.btnNumber9);
            this.Controls.Add(this.btnEqual);
            this.Controls.Add(this.btnNumber8);
            this.Controls.Add(this.btnCLear);
            this.Controls.Add(this.btnNumber0);
            this.Controls.Add(this.btnNumber7);
            this.Controls.Add(this.btnNumber6);
            this.Controls.Add(this.btnDivide);
            this.Controls.Add(this.btnMultiply);
            this.Controls.Add(this.btnMinus);
            this.Controls.Add(this.btnNumber5);
            this.Controls.Add(this.btnNumber4);
            this.Controls.Add(this.btnNumber3);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.btnNumber2);
            this.Controls.Add(this.btnNumber1);
            this.Name = "Form1";
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNumber1;
        private System.Windows.Forms.Button btnNumber4;
        private System.Windows.Forms.Button btnNumber7;
        private System.Windows.Forms.Button btnNumber2;
        private System.Windows.Forms.Button btnNumber5;
        private System.Windows.Forms.Button btnNumber8;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button btnMinus;
        private System.Windows.Forms.Button btnEqual;
        private System.Windows.Forms.Button btnNumber3;
        private System.Windows.Forms.Button btnNumber6;
        private System.Windows.Forms.Button btnNumber9;
        private System.Windows.Forms.TextBox txtDisplay;
        private System.Windows.Forms.Button btnNumber0;
        private System.Windows.Forms.Button btnCLear;
        private System.Windows.Forms.Button btnMultiply;
        private System.Windows.Forms.Button btnDivide;
    }
}

