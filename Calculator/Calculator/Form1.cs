﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        decimal decAnswer = 0.0m;
        bool bolAnswer = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtDisplay.Text = "0";
        }

        private void btn_Click(object sender, EventArgs e)
        {      
            Button btnNumber = (Button)sender;
            string currentNumber = txtDisplay.Text;

            if (currentNumber == "0" || bolAnswer == true)
            {
                currentNumber = "";
            }
            
            txtDisplay.Text = currentNumber + btnNumber.Text;
        }

        private void btnCLear_Click(object sender, EventArgs e)
        {
            txtDisplay.Text = "0";
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            string strCalculation = txtDisplay.Text;
            string[] strCalculationArray = strCalculation.Split(' ');
            if(strCalculationArray.Count() == 3)
            {
                switch (strCalculationArray[1])
                {
                    case "*":
                        decAnswer = decimal.Parse(strCalculationArray[0]) * decimal.Parse(strCalculationArray[2]);
                        break;
                    case "/":
                        if(decimal.Parse(strCalculationArray[2]) == 0)
                        {
                            decAnswer = 0;
                        }
                        else
                        {
                            decAnswer = decimal.Parse(strCalculationArray[0]) / decimal.Parse(strCalculationArray[2]);
                        }
                        break;
                    case "-":
                        decAnswer = decimal.Parse(strCalculationArray[0]) - decimal.Parse(strCalculationArray[2]);
                        break;
                    default:
                        decAnswer = decimal.Parse(strCalculationArray[0]) + decimal.Parse(strCalculationArray[2]);
                        break;
                }
                txtDisplay.Clear();
                txtDisplay.Text = decAnswer.ToString();
                bolAnswer = true;
            }     
        }

        private void btnOperator_Click(object sender, EventArgs e)
        {
            bolAnswer = false;
            string strFirstNumber = txtDisplay.Text;
            string[] strSumArray = strFirstNumber.Split(' ');
            if (strSumArray.Count() == 1)
            {
                if (strFirstNumber != "" || strFirstNumber != "0")
                {
                    Button btnOperator = (Button)sender;

                    strFirstNumber += " " + btnOperator.Text + " ";

                    txtDisplay.Text = strFirstNumber;
                }
                else
                {
                    txtDisplay.Text = "0";
                }
            }            
        }
    }
}